"""Publish a test result to OSCI brew-build.test.(running|complete|error) topics."""
import argparse
import datetime
import json
import os
import pathlib
import sys
import typing

from cki_lib import misc
from cki_lib import stomp
import koji

UMB_HOST = 'umb.api.redhat.com'
UMB_PORT = 61612


def get_brew_brew_build(nvr: str) -> dict:
    """Get information from a brew build nvr."""
    hub = koji.ClientSession(os.environ.get('KOJI_SERVER'))
    # https://github.com/koji-project/koji/blob/master/koji/__init__.py#L3139
    hub.opts['offline_retry'] = True
    # Retry for 3 hrs
    hub.opts['max_retries'] = 3*60
    hub.opts['offline_retry_interval'] = 60
    hub.opts['retry_interval'] = 60
    try:
        info = hub.getBuild(nvr)
    # pylint: disable=broad-except
    except Exception as exc:
        print('Unable to query brew', file=sys.stderr)
        print(exc, file=sys.stderr)
        raise
    return info


def create(source_package_nvr, run_info, test_info, pipeline_id, status) -> dict:
    """Create a message valid for OSCI brew-build.test.(running|complete|error) topics."""
    brew_build = get_brew_brew_build(source_package_nvr)

    owner_name = misc.get_nested_key(brew_build,
                                     'extra/custom_user_metadata/osci/upstream_owner_name')

    message = {
        'artifact': {
            'component': brew_build['name'],
            'id': brew_build['task_id'],
            'issuer': owner_name or brew_build['owner_name'],
            'nvr': source_package_nvr,
            'scratch': False,
            'source': brew_build['source'],
            'type': 'brew-build',
        },
        'contact': {
            'docs': 'https://docs.engineering.redhat.com/display/KQ/CI',
            'email': 'kernel-ci-list@redhat.com',
            'name': 'kernel-qe-ci',
            'slack': 'https://redhat.enterprise.slack.com/archives/C04N172J4MU',
            'team': 'kernel-qe',
            'url': 'https://redhat.enterprise.slack.com/archives/C04N172J4MU',
        },
        'generated_at': datetime.datetime.now(datetime.timezone.utc)
                                .replace(tzinfo=None).isoformat() + 'Z',
        'pipeline': {
            'id': pipeline_id,
            'name': 'kernel-qe-ci-gating',
        },
        'run': {
            'log': run_info['log'],
            'url': run_info['url']
        },
        'system': [],
        'test': {
            'category': test_info['category'],
            'docs': 'https://docs.engineering.redhat.com/display/KQ/CI',
            'namespace': test_info['namespace'],
            'type': test_info['type'],
            'xunit': '',
        },
        'version': '1.1.14',
    }
    if status == 'complete':
        message['test']['result'] = test_info['result']
    if status == 'error':
        message['error'] = {'reason': run_info['error_reason']}
    return message


def send(msg, topic, certificate) -> bool:
    """Send an UMB message to an specific topic."""
    try:
        stomp.StompClient(host=UMB_HOST,
                          port=UMB_PORT,
                          certfile=certificate).send_message(msg, topic)
    # pylint: disable=broad-except
    except Exception as exc:
        print('Unable to send data through UMB', file=sys.stderr)
        print(exc, file=sys.stderr)
        return False

    print('Message sent sucessfully')
    return True


def main(args: typing.Optional[typing.List[str]] = None) -> int:
    """Run the tool."""
    parser = argparse.ArgumentParser(
        description='Create a message to be sent to test.complete topic as OSCI gating message.'
    )
    parser.add_argument(
        '--source-nvr',
        dest='source_nvr',
        type=str,
        required=True,
        help='Source package NVR.'
    )
    parser.add_argument(
        '--log-url',
        dest='log_url',
        type=str,
        required=True,
        help='log of the test run.'
    )
    parser.add_argument(
        '--run-url',
        dest='run_url',
        type=str,
        required=True,
        help='URL for the test run.'
    )
    parser.add_argument(
        '--pipeline-id',
        dest='pipeline_id',
        type=str,
        required=True,
        help='Unique ID for the pipeline.'
    )
    parser.add_argument(
        '--test-category',
        dest='category',
        type=str,
        default='functional',
        help='test category result. Default: functional'
    )
    parser.add_argument(
        '--test-namespace',
        dest='namespace',
        type=str,
        default='gating',
        help='test namespace. Default: brew component.'
    )
    parser.add_argument(
        '--test-type',
        dest='type',
        type=str,
        default='tier1',
        help='test type. Default: tier1.'
    )
    parser.add_argument(
        '--test-status',
        dest='status',
        type=str,
        required=True,
        choices=['complete', 'error', 'running'],
        help='test status.'
    )
    parser.add_argument(
        '--test-result',
        dest='result',
        type=str,
        required=False,
        choices=['passed', 'failed', 'info'],
        help='test result.'
    )
    parser.add_argument(
        '--error-reason',
        dest='error_reason',
        type=str,
        required=False,
        default='Infrastructure error',
        help='Reason of the error.'
    )
    parser.add_argument(
        '--certificate',
        type=str,
        required=True,
        default=os.environ.get('UMB_CERTIFICATE'),
        help="Certificate file (pem) to send messages through UMB. "
             "Can also set via UMB_CERTIFICATE environment variable."
    )
    parser.add_argument(
        '--dry-run',
        dest='dry_run',
        action='store_true',
        help='Only print the message, does not send to UMB.'
    )
    parsed_args = parser.parse_args(args)

    if not pathlib.Path(parsed_args.certificate).is_file():
        print(f'{parsed_args.certificate} is not a file or does not exist', file=sys.stderr)
        return 1

    run_info = {'url': parsed_args.run_url, 'log': parsed_args.log_url}
    test_info = {'category': parsed_args.category,
                 'namespace': parsed_args.namespace,
                 'type': parsed_args.type,
                 'result': parsed_args.result}

    if parsed_args.status == 'error':
        run_info['error_reason'] = parsed_args.error_reason

    osci_msg = create(parsed_args.source_nvr, run_info, test_info,
                      parsed_args.pipeline_id, parsed_args.status)
    if not osci_msg:
        print("FAIL: Could not create osci message", file=sys.stderr)
        return 1

    topic = f'/topic/VirtualTopic.eng.ci.kernel-qe.brew-build.test.{parsed_args.status}'
    if parsed_args.dry_run:
        print(f'Topic: {topic}')
        print(f'Message:\n{json.dumps(osci_msg, ensure_ascii=False, indent=4)}')
    else:
        if not send(osci_msg, topic, parsed_args.certificate):
            return 1

    return 0


if __name__ == "__main__":  # pragma: no cover
    sys.exit(main(sys.argv[1:]))
