"""Command line to get the automotive compose for Testing Farm."""

import argparse
import sys
import typing

from tf_requests import get_auto_compose


def main(args: typing.List[str] | None = None) -> int:
    """Get the automotive compose for Testing Farm."""
    parser = argparse.ArgumentParser(
        description="Get the automotive compose for Testing Farm."
    )

    parser.add_argument(
        "--arch",
        type=str,
        choices=["aarch64", "x86_64"],
        default="aarch64",
        help="Arch (By default aarch64).",
    )

    parser.add_argument(
        "--hw-target",
        type=str,
        choices=["am69sk", "aws", "j784s4evm", "qemu", "rcar_s4", "ridesx4", "rpi4",
                 "s32g_vnp_rdb3"],
        default="ridesx4",
        help="HW Target (By default ridesx4).",
    )

    parser.add_argument(
        "--webserver-releases",
        type=str,
        required=True,
        help="Webserver releases URL.",
    )

    parser.add_argument(
        "--release",
        type=str,
        required=True,
        help="Release Name.",
    )

    parser.add_argument(
        "--image-type",
        type=str,
        choices=["ostree", "regular"],
        default="regular",
        help="Image Type (By default regular).",
    )

    parser.add_argument(
        "--image-name",
        type=str,
        choices=["cki", "developer", "minimal", "qa", "qm", "qm-minimal"],
        default="qa",
        help="Image name (By default qa).",
    )

    parser_args = parser.parse_args(args)

    if compose := get_auto_compose.calculate_auto_compose({
            'ARCH': parser_args.arch,
            'RELEASE_NAME': parser_args.release,
            'IMAGE_NAME': parser_args.image_name,
            'IMAGE_TYPE': parser_args.image_type,
            'HW_TARGET': parser_args.hw_target,
            'webserver_releases': parser_args.webserver_releases
    }):
        print(compose[1])
        return 0

    print("Unable to get the compose.", file=sys.stderr)
    return 1


if __name__ == "__main__":  # pragma: no cover
    sys.exit(main(sys.argv[1:]))
