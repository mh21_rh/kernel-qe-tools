"""JSON Renderer."""
import argparse
import json
import os
import pathlib
import sys
import typing

import jinja2
import yaml


def render(data: dict, template: pathlib.Path) -> str:
    """Render JSON file using a template."""
    # pylint: disable=duplicate-code
    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader(template.parent),
        trim_blocks=True,
        keep_trailing_newline=True,
        lstrip_blocks=True,
        autoescape=jinja2.select_autoescape(default_for_string=True),
        undefined=jinja2.StrictUndefined
    )
    return env.get_template(template.name).render(data, env=os.environ)


def main(args: typing.Optional[typing.List[str]] = None) -> int:
    """Command line interface to render a JSON file using a template."""
    cli_description = """
    Another opinionated Jinja Renderer.

    You can provided a YAML or JSON files to provide data, and Jinja 2
    template file.
    """.strip()
    parser = argparse.ArgumentParser(description=cli_description)
    # pylint: disable=duplicate-code
    parser.add_argument(
        '--input',
        '-i',
        action='append',
        dest='input_files',
        type=pathlib.Path,
        required=True,
        help='Input file (YAML or JSON), this paramter can be invoke several times.'
    )

    # pylint: disable=duplicate-code
    parser.add_argument(
        '--template',
        '-t',
        type=pathlib.Path,
        required=True,
        help='Template file.'
    )

    # pylint: disable=duplicate-code
    parser.add_argument(
        '--output',
        '-o',
        type=pathlib.Path,
        help='Output file, otherwise standard output.'
    )

    parsed_args = parser.parse_args(args)

    data: dict = {}
    for input_file in parsed_args.input_files:
        file_name = input_file.name.lower()
        if any(file_name.endswith(ext) for ext in ['yaml', 'yml']):
            data.update(yaml.safe_load(input_file.read_text(encoding='utf-8')))
        elif file_name.endswith('json'):
            data.update(json.loads(input_file.read_text(encoding='utf-8')))
        else:
            print(f'The file {input_file.name} will not be used.')

    content = render(data, parsed_args.template)

    if not parsed_args.output:
        print(content, end="")
    else:
        parsed_args.output.write_text(content, encoding='utf-8')

    return 0


if __name__ == "__main__":  # pragma: no cover
    sys.exit(main(sys.argv[1:]))
