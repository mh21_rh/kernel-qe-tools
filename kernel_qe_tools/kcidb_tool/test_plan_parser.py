"""Test Plan KCIDB Parser."""

from cki_lib.logger import get_logger

from . import utils
from .dataclasses import ParserArguments
from .parser import KCIDBToolParser

LOGGER = get_logger(__name__)


class TestPlanParser(KCIDBToolParser):
    """Parser to create a basic Test Plan."""
    __test__ = False

    def __init__(self, args):
        """Initialize object with the given data."""
        super_args = ParserArguments(
            brew_task_id=args.brew_task_id,
            builds_origin=args.builds_origin,
            checkout=args.checkout,
            checkout_origin=args.checkout_origin,
            contacts=[],
            extra_output_files=[],
            nvr=args.nvr,
            src_nvr=args.src_nvr,
            test_plan=True,
            tests_origin='',
            tests_provisioner_url='',
            report_rules='',
            submitter=''
        )

        super().__init__('<xml/>', super_args)
        self.arch = args.arch

    def add_checkout(self):
        """Add checkout."""
        # pylint: disable=duplicate-code
        self.report['checkout'] = utils.clean_dict({
            'id': self.checkout_id,
            'origin': self.checkout_origin,
            'valid': True,
            'misc': self.add_checkout_misc_info()
        })

    def add_build(self):
        """Add build if it is not present."""
        # pylint: disable=duplicate-code
        if not self.exist_build_by_arch():
            self.report['builds'].append({
                'id': self._get_build_id(),
                'origin': self.builds_origin,
                'checkout_id': self.checkout_id,
                'architecture': self.arch,
                'valid': True,
                'misc': self.add_build_misc_info()
            })

    def process(self):
        """Create the Test Plan."""
        self._clean_report()
        self.add_checkout()
        self.add_build()
        self.processed = True
