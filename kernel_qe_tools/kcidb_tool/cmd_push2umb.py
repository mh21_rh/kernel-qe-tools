"""Subcommand push2umb."""

import json
import os
import pathlib
import sys

from cki_lib import stomp

from . import cmd_misc

DATAWAREHOUSE_URL = 'https://datawarehouse.cki-project.org'
UMB_HOST = 'umb.api.redhat.com'
UMB_PORT = 61612
UMB_TOPIC = '/topic/VirtualTopic.eng.cki.results'


def build(cmds_parser, common_parser):
    """Build the argument parser for the create command."""
    cmd_parser, _ = cmd_misc.build(
        cmds_parser,
        common_parser,
        "push2umb",
        help_message='Send kcidb results through UMB.',
        add_subparser=False,
    )

    umb_cerficate = os.environ.get('UMB_CERTIFICATE')

    cmd_parser.description = 'Send kcidb results through UMB.'

    cmd_parser.add_argument('-c',
                            '--certificate',
                            type=str,
                            required=True,
                            default=umb_cerficate,
                            help="Certificate file (pem) to send messages through UMB. "
                                 "Can also set via UMB_CERTIFICATE environment variable."
                            )

    cmd_parser.add_argument('-i', '--input',
                            type=pathlib.Path,
                            required=False,
                            default=pathlib.Path('kcidb.json'),
                            help="The path to the kcidb file (By default kcidb.json)."
                            )


def main(args):
    """Run cli command."""
    if not args.input.is_file():
        print(f'{args.input} is not a file or does not exist', file=sys.stderr)
        sys.exit(1)
    if not pathlib.Path(args.certificate).is_file():
        print(f'{args.certificate} is not a file or does not exist', file=sys.stderr)
        sys.exit(1)

    data = json.loads(args.input.read_text(encoding='utf-8'))

    print('Sending KCIDB data through UMB')
    try:
        stomp.StompClient(host=UMB_HOST,
                          port=UMB_PORT,
                          certfile=args.certificate).send_message(data, UMB_TOPIC)
    # pylint: disable=broad-except
    except Exception as exc:
        print('Unable to send KCIDB data through UMB', file=sys.stderr)
        print(exc, file=sys.stderr)
        sys.exit(1)
    print('KCIDB data sent sucessfully')
    for checkout in data.get('checkouts'):
        print(f'DataWarehouse link: {DATAWAREHOUSE_URL}/kcidb/checkouts/{checkout["id"]}')
