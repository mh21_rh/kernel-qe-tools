"""Miscellaneous command-related definitions."""


def build(cmds_parser, common_parser, name, help_message, add_subparser=True):
    """Build a new command argument with its corresponding action subparser."""
    cmd_parser = cmds_parser.add_parser(
        name,
        help=help_message,
        parents=[common_parser],
    )
    action_subparser = None
    if add_subparser:
        action_subparser = cmd_parser.add_subparsers(
            title="action",
            dest="action",
        )
    return cmd_parser, action_subparser
