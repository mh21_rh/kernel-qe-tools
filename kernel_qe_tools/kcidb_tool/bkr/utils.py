"""Beaker utils."""

from datetime import datetime
import re
import typing
import xml.etree.ElementTree as ET

from cki_lib.kcidb.validate import sanitize_kcidb_status
from cki_lib.logger import get_logger
from cki_lib.misc import utc_now_iso

from .. import utils

LOGGER = get_logger(__name__)
DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
# abc de <abcde@redhat.com> / abcde, abc de <abcde@redhat.com>
RGX_MAINTAINERS = re.compile(r'(?:(.*?) <(.*?)>(?: / ([^ ,]+))?(?:\s*, ){0,1})')


def get_seconds(time_str):
    """Get seconds from time."""
    hours, minutes, seconds = time_str.split(':')
    return int(hours) * 3600 + int(minutes) * 60 + int(seconds)


def get_duration(bkr_duration):
    """
    Get duration of a task.

    Almost all tasks takes les than one day, using the following format:

    (h)h:mm:ss

    If the task takes more time, the format add more information, here is an
    example about it.
    2 days, 0:33:33

    This method implements the logic to manage time(hh:mm:ss), day(s) and week(s) and
    returns the number of seconds.
    """
    regex_time = r"^\d{1,2}:\d{2}:\d{2}$"
    # 99% of the cases takes less than one day, so we're going to try it first
    if re.match(regex_time, bkr_duration):
        return get_seconds(bkr_duration)

    # We're going to process the line.
    seconds = 0

    for chunk in bkr_duration.split(','):
        strip_chunk = chunk.strip()
        # First test with time
        if re.match(regex_time, strip_chunk):
            seconds += get_seconds(strip_chunk)
        # We suppose we have other units
        else:
            r_quantity, r_unit = strip_chunk.split()
            quantity = int(r_quantity)
            unit = r_unit.lower()
            if 'day' in unit:
                seconds += quantity * 86400
            elif 'week' in unit:
                seconds += quantity * 86400 * 7

    return seconds


def get_duration_between_dates(start: str, end: str) -> str:
    """Get duration between dates."""
    beaker_format_time = '%Y-%m-%d %H:%M:%S'
    start_date = datetime.strptime(start, beaker_format_time)
    end_date = datetime.strptime(end, beaker_format_time)
    return str(end_date - start_date)


def get_utc_datetime(datetime_str):
    """Get datetime in UTC format."""
    if not datetime_str:
        return utc_now_iso()
    return f'{datetime.strptime(datetime_str, DATE_FORMAT).isoformat()}+00:00'


def get_int(text):
    """Cast to int when it's possible."""
    try:
        return int(text)
    except (ValueError, TypeError):
        return None


def get_console_log_url(recipe_id):
    """Get console log."""
    return f'https://beaker.engineering.redhat.com/recipes/{recipe_id}/logs/console.log'


def get_recipe_url(recipe_id):
    """Get recipe url."""
    return f'https://beaker.engineering.redhat.com/recipes/{recipe_id}'


def get_output_console_log(recipe_id):
    """Get console log in a list."""
    return [{'name': 'console.log', 'url': get_console_log_url(recipe_id)}]


def sanitize_test_status(task):
    # pylint: disable=line-too-long
    """
    Get test status from a Beaker Task.

    TODO: Wait until https://gitlab.com/cki-project/datawarehouse/-/issues/306 is ready
    (Add support to Datawarehouse).

    At cki-lib, we have a helper function to get the status
    giving the task status, but in bkr2kcidb we need to manage
    the MISS status.

    When Beaker did not run some tests, we have to assign the MISS status,
    otherwise we'll use the cki_lib helper function.

    A MISS test is a Beaker task without *start_time*, the result is "Warn" and status is "Aborted",
    and it only has a result.

    A MISS test is a Beaker task can be a task with the "Cancelled" status.

    Below we can see an example

    <task name="task_1" role="None" version="1-33" id="10" result="Warn" status="Aborted" avg_time="7200" start_time="2023-08-28 21:50:26" finish_time="2023-08-29 00:20:55" duration="2:30:29">
      <rpm name="package_name_1" path="/mnt/tests/kernel/test1"/>
        <roles>
          <role value="None">
            <system value="server-a.redhat.com"/>
          </role>
        </roles>
        <logs>
          <log href="https://beaker.server/recipes/1/tasks/10/logs/harness.log" name="harness.log"/>
          <log href="https://beaker.server/recipes/1/tasks/10/logs/taskout.log" name="taskout.log"/>
        </logs>
        <results>
          <result path="path1" start_time="2023-08-28 21:54:03" score="None" result="Pass" id="1">None
            <logs>
              <log href="https://beaker.server/recipes/1/tasks/10/results/1/logs/avc.log" name="avc.log"/>
              <log href="https://beaker.server/recipes/1/tasks/10/results/1/logs/resultoutputfile.log" name="resultoutputfile.log"/>
            </logs>
          </result>
          <result path="path_2" start_time="2023-08-28 21:54:13" score="None" result="Pass" id="2">None
            <logs>
              <log href="https://beaker.server/recipes/1/tasks/10/results/2/logs/avc.log" name="avc.log"/>
              <log href="https://beaker.server/recipes/1/tasks/10/results/2/logs/resultoutputfile.log" name="resultoutputfile.log"/>
            </logs>
          </result>
          <result path="path_3" start_time="2023-08-28 21:54:23" score="None" result="Pass" id="3">None
            <logs>
              <log href="https://beaker.server/recipes/1/tasks/10/results/3/logs/avc.log" name="avc.log"/>
              <log href="https://beaker.server/recipes/1/tasks/10/results/3/logs/resultoutputfile.log" name="resultoutputfile.log"/>
            </logs>
          </result>
          <result path="path_4" start_time="2023-08-28 21:54:34" score="None" result="Pass" id="4">None
            <logs>
              <log href="https://beaker.server/recipes/1/tasks/10/results/4/logs/dmesg.log" name="dmesg.log"/>
              <log href="https://beaker.server/recipes/1/tasks/10/results/4/logs/avc.log" name="avc.log"/>
              <log href="https://beaker.server/recipes/1/tasks/10/results/4/logs/resultoutputfile.log" name="resultoutputfile.log"/>
            </logs>
          </result>
          <result path="/" start_time="2023-08-29 00:20:55" score="0" result="Warn" id="5">External Watchdog Expired</result>
        </results>
      </task>
      <task name="git://git_server#general/include" role="None" id="11" result="Warn" status="Aborted">
        <fetch url="git://git_server#general/include"/>
        <roles>
          <role value="None">
            <system value="server-b.redhat.com"/>
          </role>
        </roles>
        <params>
          <param name="CMD" value="script.sh"/>
          <param name="INSTALL" value="something"/>
        </params>
        <logs/>
        <results>
          <result path="/" start_time="2023-08-29 00:20:55" score="0" result="Warn" id="6">External Watchdog Expired</result>
        </results>
      </task>

    The task number 10, did not finish correctly due to the External Watchdog and the number 11
    did not start, we don't have any *start_time* and only the External Watchdog result.

    Based on this XML code, we should mark the task number 11 as MISS.

    Here we can see other example:

      <task name="UPT smoke tests aborting the recipe" role="STANDALONE" id="12" result="Fail" status="Aborted" start_time="2023-09-18 13:01:23" finish_time="2023-09-18 13:01:35" duration="0:00:12">
        <fetch url="https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/archive/main/kernel-tests-main.zip#upt_smoke_tests/abort_full_recipe"/>
        <roles>
          <role value="STANDALONE">
            <system value="sut.redhat.com"/>
          </role>
        </roles>
        <logs>
          <log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/logs/taskout.log" name="taskout.log"/>
          <log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/logs/harness.log" name="harness.log"/>
        </logs>
        <results>
          <result path="Setup" start_time="2023-09-18 13:01:28" score="0" result="Pass" id="19">None<logs><log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/results/19/logs/avc.log" name="avc.log"/><log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/results/19/logs/resultoutputfile.log" name="resultoutputfile.log"/></logs></result>
          <result path="First-test" start_time="2023-09-18 13:01:30" score="0" result="Pass" id="777099241">None<logs><log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/results/777099241/logs/avc.log" name="avc.log"/><log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/results/777099241/logs/resultoutputfile.log" name="resultoutputfile.log"/></logs></result>
          <result path="UPT smoke tests aborting the recipe" start_time="2023-09-18 13:01:32" score="1" result="Fail" id="777099247">None<logs><log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/results/777099247/logs/avc.log" name="avc.log"/><log href="https://beaker.engineering.redhat.com/recipes/10/tasks/12/results/777099247/logs/resultoutputfile.log" name="resultoutputfile.log"/></logs></result>
          <result path="/" start_time="2023-09-18 13:01:35" score="0" result="Warn" id="20">None</result>
        </results>
      </task>
      <task name="Sleep 60 seconds" role="STANDALONE" id="13" result="Warn" status="Aborted">
        <fetch url="https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/archive/main/kernel-tests-main.zip#distribution/command"/>
        <roles>
          <role value="STANDALONE">
            <system value="sut.redhat.com"/>
          </role>
        </roles>
        <params>
          <param name="TESTARGS" value="sleep 60"/>
        </params>
        <logs>
          <log href="https://beaker.engineering.redhat.com/recipes/10/tasks/13/logs/harness.log" name="harness.log"/>
        </logs>
        <results>
          <result path="/" start_time="2023-09-18 13:01:36" score="0" result="Warn" id="21">None</result>
        </results>
      </task>

    In this recipe has been aborted by the task 12, and the task number 13 never has been executed.
    The task 13, does not have *start_time* and the status and result are the expected ones with one
    result.

      <task name="distribution/check-install" role="STANDALONE" id="180572517" result="Warn" status="Cancelled">
        <fetch url="https://github.com/beaker-project/beaker-core-tasks/archive/master.zip#check-install"/>
        <roles>
          <role value="STANDALONE"/>
        </roles>
        <logs/>
        <results>
          <result path="/" start_time="2024-07-15 01:06:10" score="0" result="Warn" id="841661045">None</result>
        </results>
      </task>

    This task never could be executed, so it will mark as MISS
    """  # noqa
    # Check for miss status
    if task.get('status', '') == 'Cancelled':
        return 'MISS'

    results = task.findall('.//result')
    if not task.get('start_time') and \
            task.get('result') == 'Warn' and \
            task.get('status') == 'Aborted' and \
            len(results) == 1:
        return 'MISS'

    # No MISS status, use the cki_lib helper
    return sanitize_kcidb_status(task.get('result'))


def get_tasks_maintainers(task):
    """
    Get the list of maintainers of a task.

    The maintainers must be set as a task parameter with the name 'MAINTAINERS'.
    The value should have this format.

    user_1 <user_1@email.com> / giltab_user_1, user_2 <user_2@email.com>

    That value represents a list of two maintainers, the first one has a gitlab user_name.

    The gitlab information is optional, but the slash character is mandatory to add
    the gitlab username.
    """
    maintainers = []

    for param in task.findall('.//param'):
        if param.get('name') == 'MAINTAINERS':
            for name, email, gitlab in re.findall(RGX_MAINTAINERS, param.get('value')):
                maintainers.append(
                    utils.clean_dict({'name': name, 'email': email, 'gitlab': gitlab})
                )

    return maintainers


def get_results(test, results):
    """
    Generate results for a test.

    We can process directly all results except results added by Beaker on
    top the Restraint results.

    When a test is Aborted, Beaker always add a result following this criteria:
    * The path field is equal to '/'
    * The result field is equal to 'Warn' or 'Panic'
    * The result contains a text with the Beaker Information

    If a status of the test is 'MISS', all subtest will use 'MISS' as well.
    """
    all_results = []
    for number, result in enumerate(results.iter('result'), 1):
        path = result.get('path')
        _result = result.get('result')

        if path == '/' and _result in ['Warn', 'Panic']:
            name = result.text
        else:
            name = path

        if test['status'] == 'MISS':
            subtest_status = test['status']
            LOGGER.info('Forcing subtest(%s).status=MISS because test.status=MISS',
                        f'{test["id"]}.{number}')
        else:
            subtest_status = sanitize_kcidb_status(_result)

        all_results.append(utils.clean_dict({
            'id': f'{test["id"]}.{number}',
            'name': name or 'UNDEFINED',  # deprecated
            'comment': name or 'UNDEFINED',
            'status': subtest_status,
            'output_files': utils.get_output_files(result.find('logs')),
        }))

    return all_results


def create_system_provision_task(recipe, test_id):
    """
    Add a fake provision "beaker" task to every recipe.

    This test shows the result of the provision. A Beaker recipe has
    the installation keyword where we can get some information.

    The installation keyword can have these properties:
      * install_started
      * install_finished
      * postinstall_finished (after the post install tasks)

    At least, we've identified these scenarios

    * The installation and tests were executed: In this case, we can get the property
      postinstall_finished inside the installation tag, and at least one was executed.
      The status should be PASS.

    * The installation was canceled: In this case, the installation tag does not have any
      property, The status should ERROR.

    * The installation failed, in that case the have the property install_started but we don't
      have the postinstall_finished property. The status should be ERROR.

    * The installation worked but the system could not boot after that. In this case the
      installation tag has all property but all tests are MISS. The status should be ERROR.
    """
    task = ET.fromstring('<task />')
    task_result = 'PASS'

    # Add basic information
    task.set('id', str(test_id))
    task.set('name', 'system_provision')

    # Add recipe logs
    task.append(recipe.find('logs'))

    # Add start time
    installation = recipe.find('installation')
    if 'install_started' in installation.attrib:
        task.set('start_time', installation.get('install_started'))
    elif 'start_time' in recipe.attrib:
        task.set('start_time', recipe.get('star_time'))

    # Result
    if (
        # If the installation object does not contain a postinstall_finished attrib
        "postinstall_finished" not in installation.attrib
        # or all task for this recipe are MISS the result is ERROR
        or all(sanitize_test_status(task) == "MISS" for task in recipe.iter("task"))
    ):
        task_result = "ERROR"

    # Duration
    if 'postinstall_finished' in installation.attrib:
        task.set('duration',
                 get_duration_between_dates(task.get('start_time'),
                                            installation.get('postinstall_finished')
                                            ))
    elif 'duration' in recipe.attrib:
        task.set('duration', recipe.get('duration'))

    # Logic for the result path
    # Some logic is duplicated here, but the goal is different
    if not task.get('start_time', None):
        path = 'Cancelled before running'
    elif 'postinstall_finished' not in installation.attrib:
        path = 'Installation failed'
    elif all(sanitize_test_status(task) == "MISS" for task in recipe.iter("task")):
        path = 'All tasks missed'
    else:
        path = 'Setup OK'

    # Results
    results = ET.fromstring('<results />')
    result = ET.fromstring('<result />')
    result.set('path', path)
    result.set('result', task_result)
    result.text = 'None'
    result.append(recipe.find('logs'))
    results.append(result)
    task.append(results)

    # Add extra information
    task.set('fake', 'True')  # This is a string not a boolean

    # Add result
    task.set('result', task_result)

    return task


def post_process_results(results: typing.List[dict]) -> typing.List[dict]:
    """
    Do a post processing of a list of results (subtest of a test).

    Something, we have to do after the results are processed, and we want to keep
    clean to logic for creating results.

    Here you can see the list of things we can do here:

    * Omit External Watchdog Expired result if a Kernel Panic is present.

    At the end, we have to be sure all ids are consecutive.
    """
    fix_ids = False
    original_len = len(results)
    # Omit External Watchdog Expired result if a Kernel Panic is present.
    if any(result['status'] == 'FAIL' and result['name'] == 'Panic: Kernel panic'
           for result in results):
        results = [result for result in results if result['status'] != 'ERROR'
                   and result['name'] != 'Warn: External Watchdog Expired']
        if len(results) != original_len:
            fix_ids = True
            LOGGER.debug(
                'Omitting External Watchdog Expired result because a Kernel Panic is present'
            )

    if fix_ids:
        LOGGER.debug('Some results have been changed, fixing ids')
        for new_id, result in enumerate(results, 1):
            result['id'] = re.sub(r'\.(\d+)$', f'.{new_id}', result['id'])

    return results
